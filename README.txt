Module description
==================

This is a simple example app to highlight what you need to do in order
to create apps for use via the apps module. This module doesn't even try
to do anything worthwhile when installed, but should provide a fairly
straightforward example of what you can and should do in order to create
an app.

Conceptually an app is a wrapper for installing some other sort of 
functionality (think modules or features). This module installs the wordfilter
module as a simple example to show creating an app. It is hoped that this
can provide a basis for you to create more complex and useful apps.

Installation and Configuration
==============================

This module assumes that you have the apps module installed. Without it,
this module has no real use. If you haven't installed the apps module, do
that before proceeding. You'll also need to make directories web-server
writeable in order to do installation (whichever /sites/<sitename> directory
that you're using plus wherever you're going to install modules,
e.g. /sites/all/modules)

Once you have the apps module installed, you'll want to ensure that you
have enabled the development console for apps. @see: /admin/config/system/apps

Go to /admin/apps/development, and you should see Example App available for
installation.

In brief:
1) Install apps module.
2) Make web-server writeable /sites/all/modules & /sites/<sitename> directories.
3) Enabled the dev console: /admin/config/system/apps
4) See the app: /admin/apps/development

Important areas of note (or how to build an app)
================================================

Pay special attention to the following things:
1) example_app.info
 There are 2 additional lines added to this file for apps distribution/testing.
  apps[server] = "development"
  apps[manifests][] = "app/manifest.app"
 These lines define that we're using the development app server (so we can test
 the app locally and vs, for instance, the openpublic app server) and provide
 the relative path to our manifest file.
 
2) app/manifest.app
 This is the core file for defining all of the meaningful components of the app.
 Each line is discussed in the file, but in short it provides all the text,
 and urls necessary for your app to install.
 
 3) example_app.module
  This is the final layer and provides the optional components of your app. In
  here, we've provided some examples of how to get a configuration form set up,
  and what would need to be done in order to provide demo content.
  
This system relies heavily on the apps module which is still in active
development and provides an overview of some of the most used functionality.
As things change or if you need further information, please see
http://community.aegirproject.org/handbook/strategy/open-app-standard-draft#How_do_you_create_an_app
for additional information.
